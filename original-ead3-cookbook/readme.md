EAD3 Cookbook Files
===================

* Author: Michael Fox
* Beta Release Date: 2014-09-04

List of Files
-------------

* [EAD3toPDF.xsl](https://github.com/saa-ead-roundtable/ead3-stylesheets/original-ead3-cookbook/EAD3toPDF.xsl): a beta stylesheet to transform EAD3 to PDF. This proof-of-concept XSLT 1.0 stylesheet can be used to create an XML document encoded according to the XSL Formatting Object (XSL:FO, or just FO) specification. A FO document instance is the format that almost all formatting object processors use to create PDF, RTF or other "for print" file formats.
